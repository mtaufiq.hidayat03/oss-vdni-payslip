<?php
$pecah = explode("-", $cek->periode);
$bulan = $pecah[1];
$kurang = $bulan - 1;
if($kurang == "0"){
    $bln_kemarin = 12;
} else {
    $bln_kemarin = $kurang;
}
if($bln_kemarin == "01"){
    $nm_bln1 = "JANUARI";
} else if($bln_kemarin == "02") {
    $nm_bln1 = "FEBRUARI";
} else if($bln_kemarin == "03") {
    $nm_bln1 = "MARET";
}  else if($bln_kemarin == "04") {
    $nm_bln1 = "APRIL";
}  else if($bln_kemarin == "05") {
    $nm_bln1 = "MEI";
}  else if($bln_kemarin == "06") {
    $nm_bln1 = "JUNI";
}  else if($bln_kemarin == "07") {
    $nm_bln1 = "JULI";
}  else if($bln_kemarin == "08") {
    $nm_bln1 = "AGUSTUS";
}  else if($bln_kemarin == "09") {
    $nm_bln1 = "SEPTEMBER";
}  else if($bln_kemarin == "10") {
    $nm_bln1 = "OKTOBER";
}  else if($bln_kemarin == "11") {
    $nm_bln1 = "NOVEMBER";
}  else if($bln_kemarin == "12") {
    $nm_bln1 = "DESEMBER";
}
$thn = $pecah[0];
if($bulan == "01"){
    $nm_bln = "JANUARI";
} else if($bulan == "02") {
    $nm_bln = "FEBRUARI";
} else if($bulan == "03") {
    $nm_bln = "MARET";
}  else if($bulan == "04") {
    $nm_bln = "APRIL";
}  else if($bulan == "05") {
    $nm_bln = "MEI";
}  else if($bulan == "06") {
    $nm_bln = "JUNI";
}  else if($bulan == "07") {
    $nm_bln = "JULI";
}  else if($bulan == "08") {
    $nm_bln = "AGUSTUS";
}  else if($bulan == "09") {
    $nm_bln = "SEPTEMBER";
}  else if($bulan == "10") {
    $nm_bln = "OKTOBER";
}  else if($bulan == "11") {
    $nm_bln = "NOVEMBER";
}  else if($bulan == "12") {
    $nm_bln = "DESEMBER";
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>SLIP GAJI</title>
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		} @font-face {
            font-family: SimHei;
            src: url('../storage/fonts/SimHei.ttf') format('truetype');
         }

         * {
           font-family: SimHei;
         }
	</style>
<div>
	<table style="width:65%;" align="left">
        <tr>
            <td>NAMA</td>
            <td>:</td>
            <td>{{ $cek->data_karyawan->nama }}</td>
        </tr>
        <tr>
            <td>NIK</td>
            <td>:</td>
            <td>{{ $cek->data_karyawan->nik }}</td>
        </tr>
        <tr>
            <td>DEPARTEMEN</td>
            <td>:</td>
            <td>{!! $cek->departement !!}</td>
        </tr>
        <tr>
            <td>DIVISI</td>
            <td>:</td>
            <td>{{ $cek->divisi }}</td>
        </tr>
        <tr>
            <td>POSISI</td>
            <td>:</td>
            <td>{{ $cek->posisi }}</td>
        </tr>
        <tr>
            <td>SALARY STATUS</td>
            <td>:</td>
            <td>{{ $cek->status_salary }}</td>
        </tr>
    </table>

    <table style="width:30%;" align="right" >
        <tr>
            <td><b>PRIBADI DAN RAHASIA</b></td>
        </tr>
        <tr>
            @if($cek->data_karyawan->nm_perusahaan == "OSS")
                <td><img src="../assets/images/logo-dark.png" alt="" height="22"></td>
            @elseif($cek->data_karyawan->nm_perusahaan == "PMS")
                <td><img src="../assets/images/logo-pms.png" alt="" height="30"></td>
            @else
                <td><img src="../assets/images/logo-dark.png" alt="" height="22"></td>
            @endif
        </tr>
        <tr>
            <td>SLIP GAJI {{ $nm_bln }} {{ $thn }}</td>
        </tr>
        <tr>
            <td>( PERIODE 21 {{ $nm_bln1 }} - 20 {{ $nm_bln }} )</td>
        </tr>
    </table>
     <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <table style="width:49%;" align="left">
        <tr>
            <td style="width: 185px">Jumlah Hari Kerja</td>
            <td style="width: 15px">:</td>
            <td>{{ $cek->jml_hari_kerja }} Hari</td>
        <tr>
   </table>
   <table style="width:49%;" align="right">
        @if(!empty($cek->jml_hour_machine))
        <tr>
            <td style="width: 160px">Jumlah Hour Machine</td>
            <td style="width: 15px">:</td>
            <td>{{ $cek->jml_hour_machine }} Jam</td>
        <tr>
        @endif
</table>
   <br>
   <br>
   <table style="width:49%;" align="left">
        <tr>
            <td colspan="3"><b>RINCIAN GAJI :</b></td>
        </tr>
        
        <tr>
            <td>GAJI POKOK</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->gaji_pokok) }} </td>
        </tr>
        @if(!empty($cek->tunj_um))
        <tr>
            <td>TUNJANGAN UANG MAKAN</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->tunj_um) }}</td>
        </tr>
        @endif
        @if(!empty($cek->bonus))
        <tr>
            <td>TUNJANGAN MANAGERIAL</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->bonus) }}</td>
        </tr>
        @endif
        @if($cek->tunj_koefisien)
        <tr>
            <td>TUNJANGAN KOEFISIEN JABATAN </td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->tunj_koefisien) }}</td>
        </tr>
        @endif
        @if(!empty($cek->tunj_masa_kerja))
        <tr>
            <td>TUNJANGAN MASA KERJA</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->tunj_masa_kerja) }}</td>
        </tr>
        @endif
        @if(!empty($cek->thr))
        <tr>
            <td>THR / BONUS</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->thr) }}</td>
        </tr>
        @endif
        @if(!empty($cek->tunj_lapangan))
        <tr>
            <td>TUNJANGAN LAPANGAN</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->tunj_lapangan) }}</td>
        </tr>
        @endif
        @if(!empty($cek->overtime))
        <tr>
            <td>OVERTIME</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->overtime) }}</td>
        </tr>
        @endif
        @if(!empty($cek->hour_machine))
        <tr>
            <td>HOUR MACHINE</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->hour_machine) }}</td>
        </tr>
        @endif
        @if(!empty($cek->insentif))
        <tr>
            <td>INSENTIF</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->insentif) }}</td>
        </tr>
        @endif
        @if(!empty($cek->insentif_tidak_tetap))
        <tr>
            <td>INSENTIF TIDAK TETAP</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->insentif_tidak_tetap) }}</td>
        </tr>
        @endif
        @if(!empty($cek->rapel))
        <tr>
            <td>RAPEL</td>
            <td>:</td>
            <td>Rp. {{ number_format($cek->rapel) }}</td>
        </tr>
        @endif
    </table>

    <table style="width:49%;" align="right">
        <tr>
            <td colspan="3"><b>POTONGAN :</b></td>
        </tr>
     <tr>
         <td>BPJS TK JHT</td>
         <td>:</td>
         <td>Rp. {{ number_format($cek->bpjs_tk_jht) }} </td>
     </tr>
     <tr>
         <td>BPJS TK JP</td>
         <td>:</td>
         <td>Rp. {{ number_format($cek->bpjs_tk_jp) }}</td>
     </tr>
     <tr>
         <td>BPJS KESEHATAN</td>
         <td>:</td>
         <td>Rp. {{ number_format($cek->bpjs_kes) }}</td>
     </tr>
     
     <tr>
         <td>DEDUCTION</td>
         <td>:</td>
         <td>Rp. {{ number_format($cek->deduction) }}</td>
     </tr>
     <tr>
         @if($cek->durasi_sp == "1970-01-01")
             <?php $durasi_sp = ""; ?>
         @else
             <?php $durasi_sp =$cek->durasi_sp; ?>
         @endif
         <td>DURASI SURAT PERINGATAN</td>
         <td>:</td>
         <td>{{ $durasi_sp }}</td>
     </tr>

     <tr>
         <td>TOTAL DITERIMA</td>
         <td>:</td>
         <td><b>Rp. {{ number_format($cek->tot_diterima) }} </b></td>
     </tr>
    <tr>
        <td></br></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></br></td>
        <td></td>
        <td></td>
    </tr>
     <tr>
         <td></td>
         <td></td>
         <td>MOROSI, 31 {{ $nm_bln }} {{ $thn }}</td>
     </tr>
     <tr>
         <td>DI TRANSFER KE</td>
         <td></td>
         <td>DI BUAT OLEH,</td>
     </tr>
     <tr>
         <td>{{ $cek->bank_number }}</td>
         <td></td>
         <td></td>
     </tr>
     <tr>
         <td>{{ $cek->bank_name }}</td>
         <td></td>
         <td>PAYROLL</td>
     </tr>
</table>
</body>
</html>
