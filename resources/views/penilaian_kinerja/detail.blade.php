@extends('layouts.app')
@section('content')
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Pay Slip</a></li>
                                <li class="breadcrumb-item active">Kinerja Karyawan dan Pencapaian Kerja</li>
                            </ol>
                        </div>
                        <h4 class="page-title">
                                Kinerja Karyawan dan Pencapaian Kerja 员工绩效和工作成就
                        </h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <h5>
                                    FORMULIR PENILAIAN KINERJA TAHUNAN KARYAWAN<br/>
                                    PERIODE JANUARI - DESEMBER 2022<br/>
                                    <span class="chinesse">2022年1至12月的员工年度绩效评估表</span>
                                </h5>
                            </center>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <td width="18%">
                                            NAMA <br/><span class="chinesse">员工姓名</span>
                                        </td>
                                        <td width="20%" style="text-align: left; vertical-align: middle">
                                            {{ $data->data_karyawan->nama }}
                                        </td>
                                        <td width="10%">
                                            PERUSAHAAN <br/><span class="chinesse">公司</span>
                                        </td>
                                        <td width="22%" style="text-align: left; vertical-align: middle">
                                            @if($data->data_karyawan->nm_perusahaan === "OSS")
                                                PT. Obsidian Stainless Stell (OSS)
                                            @elseif($data->data_karyawan->nm_perusahaan === "PMS")
                                                PT. Pelabuhan Muara Sampara (PMS)
                                            @endif
                                        </td>
                                        <td width="10%">
                                            POSISI <br/><span class="chinesse">岗位</span>
                                        </td>
                                        <td width="15%" style="text-align: left; vertical-align: middle">
                                            @if($datasupport != null)
                                                {{ $datasupport->posisi }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            NIK <br/><span class="chinesse">工号</span>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle">
                                            {{ $data->data_karyawan->nik }}
                                        </td>
                                        <td>
                                            DEPARTEMEN <br/><span class="chinesse">大部门</span>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle">
                                            @if($datasupport != null)
                                                {{ $datasupport->departement }}
                                            @endif
                                        </td>
                                        <td>
                                            NPWP <br/><span class="chinesse">交税号</span>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle">
                                            {{ $data->data_karyawan->npwp }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            TANGGAL MASUK KERJA<br/>
                                            <span class="chinesse">入职时间</span>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle">
                                            {{ date('d M Y', strtotime($data->data_karyawan->tgl_join)) }}
                                        </td>
                                        <td>
                                            DIVISI <br/><span class="chinesse">部门</span>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle">
                                            @if($datasupport != null)
                                                {{ $datasupport->divisi }}
                                            @endif
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end .table-responsive-->
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            <!-- end row-->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5> I. EVALUASI PENCAPAIAN KINERJA (A)<br/>
                                <span class="hanging chinesse">绩效成就评价</span></h5>
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <td colspan="2" style="text-align: center">
                                            ASPEK PENILAIAN<br/>
                                            <span class="chinesse">评估项目</span>
                                        </td>
                                        <td rowspan="2" style="text-align: center; vertical-align: middle;">
                                            I. PENCAPAIAN KINERJA <span class="chinesse">业绩成就</span><br/>
                                            <span class="hanging">
                                                BOBOT PENILAIAN <span class="chinesse">评估的百分比</span> (45%)
                                            </span>
                                        </td>
                                        <td rowspan="2" style="text-align: center; vertical-align: middle;">
                                            KOLOM CENTANG (√)<br/>
                                            <span class="chinesse">评分栏</span> (√)
                                        </td>
                                        <td rowspan="2" style="text-align: center; vertical-align: middle;">
                                            KOLOM NILAI (DIISI OLEH HRD)<br/>
                                            <span class="chinesse">成绩栏（人事审核人）</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            JENIS KINERJA<br/>
                                            <span class="chinesse">业绩类型</span>
                                        </td>
                                        <td>
                                            RINCIAN KINERJA<br>
                                            <span class="chinesse">业绩细节</span>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td rowspan="15">
                                            <span class="first-line">I. SIKAP KERJA</span><br>
                                            <span class="hanging chinesse">工作态度</span>
                                        </td>
                                        <td rowspan="5">
                                                <span class="first-line">
                                                    1. Rasa Tanggung Jawab
                                                </span><br/>
                                            <span class="hanging chinesse">责任心</span>
                                        </td>
                                        <td>
                                            Mampu meminimalkan kesalahan juga memiliki rasa tanggung jawab yang
                                            tinggi.<br/>
                                            <span class="chinesse">能够最大限度地减少错误，责任心较强</span>
                                        </td>
                                        <td>
                                            <center>@if($data->rasa_tanggung_jawab == 43.5)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;">
                                            <b>{{ $data->rasa_tanggung_jawab }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Menunggu arahan dalam melakukan pekerjaan dan selalu mengutamakan pekerjaan.<br/>
                                            <span class="chinesse">等待指示做工作工作不太主动 </span>
                                        </td>
                                        <td>
                                            <center> @if($data->rasa_tanggung_jawab == 28.5)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Melakukan pekerjaan setelah menyelesaikan kepentingan pribadi serta
                                            bertanggung jawab akan pekerjaan.<br/>
                                            <span class="chinesse">先做完个人事情才工作, 有一定责任感</span>
                                        </td>
                                        <td>
                                            <center>@if($data->rasa_tanggung_jawab == 33)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Selalu menyelesaikan pekerjaan yang diberikan walaupun masih terdapat
                                            kesalahan atau menunggu perintah dalam bekerja.<br/>
                                            <span
                                                class="chinesse">虽然工作不够主动和存在返工但是总是能够把工作任务做完成</span>
                                        </td>
                                        <td>
                                            <center>@if($data->rasa_tanggung_jawab == 36)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Menyelesaikan pekerjaan tanpa menunggu arahan dan teliti dalam bekerja.<br/>
                                            <span class="chinesse">能够完成工作，工作非常主动, 细节做工作</span>
                                        </td>
                                        <td>
                                            <center> @if($data->rasa_tanggung_jawab == 49)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5">
                                            <span class="first-line">2. Kedisiplinan</span><br/>
                                            <span class="hanging chinesse">勤勉性</span>
                                        </td>
                                        <td>
                                            Memahami seluruh peraturan perusahaan meski tidak acuh terhadap peraturan
                                            tersebut<br/>
                                            <span class="chinesse">虽然很了解公司的规章但不遵守规章制度</span>
                                        </td>
                                        <td>
                                            <center>@if($data->kedisiplinan == 15)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;">
                                            <b> {{ $data->kedisiplinan }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cukup mampu mengikuti peraturan perusahaan<br/>
                                            <span class="chinesse">基本上能遵守规章制度</span>
                                        </td>
                                        <td>
                                            <center>@if($data->kedisiplinan == 19.5)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Enggan melakukan segala bentuk pelaggaran terhadap peraturan perusahaan
                                            <br/>
                                            <span class="chinesse">严格遵守规章制度</span>
                                        </td>
                                        <td>
                                            <center>@if($data->kedisiplinan == 22.5)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mematuhi peraturan perusahaan yang berlaku<br/>
                                            <span class="chinesse">能较好遵守规章制度</span>
                                        </td>
                                        <td>
                                            <center>@if($data->kedisiplinan == 21)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Kadang melakukan pelanggaran<br/>
                                            <span class="chinesse">有时违反规章制度现象</span>
                                        </td>
                                        <td>
                                            <center>@if($data->kedisiplinan == 18)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5">
                                                <span class="first-line">
                                                    3. Etika Kerja dan Dedikasi Kerja
                                                </span><br>
                                            <span class="hanging chinesse">
                                                    职业道德和工作奉献
                                                </span>
                                        </td>
                                        <td>
                                            Mengerjakan pekerjaan yang diperintahkan walau hasil kerja kurang baik dan
                                            kurang rasa ketertarikan yang terhadap pekerjaan baru.
                                            <br/>
                                            <span class="chinesse">
                                                    虽然工作质量不太好但能把工作任务做完， 对新的工作没有兴趣
                                                </span>
                                        </td>
                                        <td>
                                            <center><?php if ($data->etika_kerja == 27): ?>
                                                &#10004; <?php endif; ?> </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;">
                                            <b> <?php echo e($data->etika_kerja); ?> </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mampu berkomunikasi dengan baik guna untuk menyelesaikan pekerjaan agar
                                            cepat selesai.
                                            <br/>
                                            <span class="chinesse">
                                                    能够很好跟其他相关工作的同事沟通也能把工作任务快完成
                                                </span>
                                        </td>
                                        <td>
                                            <center><?php if ($data->etika_kerja == 49): ?>
                                                &#10004; <?php endif; ?></center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mengetahui jobdesk yang berlaku serta bertanggung jawab dengan pekerjaan
                                            yang diberikan.
                                            <br/>
                                            <span class="chinesse">
                                                    了解工作细节和技能也有一定责任感
                                                </span>
                                        </td>
                                        <td>
                                            <center><?php if ($data->etika_kerja == 33): ?>
                                                &#10004; <?php endif; ?></center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mengerjakan pekerjaan dengan terburu-buru namun mempu memahami intsruksi
                                            yang diberikan.
                                            <br/>
                                            <span class="chinesse">
                                                    做任务时太急不太稳定但能够理解上级的指令
                                                </span>
                                        </td>
                                        <td>
                                            <center><?php if ($data->etika_kerja == 19): ?>
                                                &#10004; <?php endif; ?> </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Memiliki target dalam bekerja serta menyelesaikan pekerjaan dengan maksimal.
                                            <br/>
                                            <span class="chinesse">
                                                    有目标和工作计划也能够最大限度地完成工作
                                                </span>
                                        </td>
                                        <td>
                                            <center><?php if ($data->etika_kerja == 60): ?>
                                                &#10004; <?php endif; ?> </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="15">
                                                <span class="first-line">
                                                    II. Kemampuan Dalam Bekerja
                                                </span><br/>
                                            <span class="hanging chinesse">工作能力</span>
                                        </td>
                                        <td rowspan="5">
                                                <span class="first-line">
                                                  1. Pengetahuan Profesi
                                                </span><br>
                                            <span class="hanging chinesse">
                                                    专业知识
                                                </span>
                                        </td>
                                        <td>
                                            Dapat diandalkan dalam mengerjakan pekerjaan namun tidak tertarik dengan
                                            pekerjaan baru
                                            <br/>
                                            <span class="chinesse">
                                                    能把工作做好， 工作可靠但对新工作根本没兴趣
                                                </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengetahuan_profesi == 19)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;">
                                            <b> {{ $data->pengetahuan_profesi }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cukup paham akan alur kerja dan kadang masih membutuhkan bantuan rekan kerja
                                            untuk menyelesaikan pekerjaan
                                            <br/>
                                            <span class="chinesse">
                                                    基本上了解工作流程，有时需要同事的帮助完成工作
                                                </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengetahuan_profesi == 25)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Memahami alur dan proses pekerjaan dan mampu meminimalkan permasalahan
                                            <br/>
                                            <span class="chinesse">
                                                    了解工作流程，能够最大限度地减少错误
                                                </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengetahuan_profesi == 35)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Kurang pemahaman akan alur kerja dan membutuhkan bantuan rekan kerja untuk
                                            menyelesaikan pekerjaan
                                            <br/>
                                            <span class="chinesse">
                                                    不怎么了解工作流程也需要同事的帮助完成工作
                                                </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengetahuan_profesi == 24)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mempunyai ide dalam mengerjakan pekerjaan serta paham akan alur dan proses
                                            pekerjaan
                                            <br/>
                                            <span class="chinesse">
                                                常有创新， 对本职工作流程非常了解
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengetahuan_profesi == 42)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5">
                                                <span class="first-line">
                                                    2. Kemampuan Pengambilan Keputusan
                                                </span><br/>
                                            <span class="hanging chinesse">
                                                    判断能力
                                                </span>
                                        </td>
                                        <td>
                                            Mengambil keputusan berdasarkan emosi yang terkadang kurang objektif
                                            <br/>
                                            <span class="chinesse">
                                                根据有时不太客观的情绪做出决定
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengambilan_keputusan == 14)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;" >
                                            <b> {{ $data->pengambilan_keputusan }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Berkoordinasi dengan pihak terkait dalam proses pengambilan keputusan
                                            <br/>
                                            <span class="chinesse">
                                                在决策过程中与相关方进行协调
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengambilan_keputusan == 21)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mengambil keputusan dengan spontan ketika terjadi masalah tanpa menganalisa
                                            risiko terlebih dahulu
                                            <br/>
                                            <span class="chinesse">
                                                当问题发生时不先分析就自发地做出决定
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengambilan_keputusan == 13)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Memahami serta menganalisa situasi dan kondisi ketika terjadi masalah
                                            <br/>
                                            <span class="chinesse">
                                                先理解和分析问题发生时的情况和条件
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengambilan_keputusan == 15)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Hanya menunggu keputusan dari atasan ketika terjadi masalah
                                            <br/>
                                            <span class="chinesse">
                                                有问题就等上级决定
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengambilan_keputusan == 6)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5">
                                            <span class="first-line">
                                                3. Kemampuan Pemahaman Dalam Bekerja
                                            </span> <br/>
                                            <span class="hanging chinesse">
                                                学习能力
                                            </span>
                                        </td>
                                        <td>
                                            Kurang memahami arahan sehingga membutuhkan bantuan dalam menyelesaikan
                                            pekerjaan
                                            <br/>
                                            <span class="chinesse">
                                                缺乏指令和工作流程的理解，因此在完成工作时需要帮助
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pemahaman_dalam_bekerja == 30)
                                                    &#10004;
                                                @endif  </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;">
                                            <b> {{ $data->pemahaman_dalam_bekerja }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tidak mau mengerjakan pekerjaan diluar dari job desc dan jam kerja
                                            <br/>
                                            <span class="chinesse">
                                                不愿意在日常工作任务和工作时间之外做工作
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pemahaman_dalam_bekerja == 36)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mengerti arahan serta prosedur juga memberikan ilmu kepada rekan kerja
                                            <br/>
                                            <span class="chinesse">
                                                理解指令和工作流程也为同事提供知识
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pemahaman_dalam_bekerja == 45)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Hanya mengerjakan pekerjaan rutin yang dilakukan setiap hari
                                            <br/>
                                            <span class="chinesse">
                                                只是做每天做的日常工作
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pemahaman_dalam_bekerja == 39)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Dapat mengerti arahan serta prosedur yang berlaku dalam menyelesaikan
                                            pekerjaan
                                            <br/>
                                            <span class="chinesse">
                                                理解适用于完成工作的指示和流程
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pemahaman_dalam_bekerja == 42)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="20">
                                            <span class="first-line">
                                                III. Kemampuan Menyelesaikan Pekerjaan
                                            </span><br/>
                                            <span class="hanging chinesse">
                                                工作完成 情况
                                            </span>
                                        </td>
                                        <td rowspan="5">
                                            <span class="first-line">
                                                1. Pengendalian diri dalam bekerja
                                            </span><br/>
                                            <span class="hanging chinesse">
                                                工作中的 自我控制
                                            </span>
                                        </td>
                                        <td>
                                            Tidak meninggalkan pekerjaan  sebelum pekerjaan selesai
                                            <br/>
                                            <span class="chinesse">
                                                不离开工作如果还没做完
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengendalian_diri == 28)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;">
                                            <b> {{ $data->pengendalian_diri }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Dapat menyelesaikan pekerjaan meski kurang maksimal
                                            <br/>
                                            <span class="chinesse">
                                                基本上能够完成工作，工作成果小于最大值
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengendalian_diri == 26)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Menyelesaikan pekerjaan walau dengan sesuka hati
                                            <br/>
                                            <span class="chinesse">
                                                能够完成任务， 随意工作
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengendalian_diri == 24)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Menunda-nunda pekerjaan yang sudah diperintahkan
                                            <br/>
                                            <span class="chinesse">
                                                常推迟工作工
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengendalian_diri == 20)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Menyelesaikan pekerjaan sesuai dengan prioritasnya
                                            <br/>
                                            <span class="chinesse">
                                                按优先顺序完成工作
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->pengendalian_diri == 30)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5">
                                            <span class="first-line">
                                                2. Kualitas Kerja
                                            </span><br/>
                                            <span class="hanging chinesse">
                                                工作质量
                                            </span>
                                        </td>
                                        <td>
                                            Kualitas kerja bagus juga menjadi panutan dalam tim dan lingkungan
                                            <br/>
                                            <span class="chinesse">
                                                工作质量良好， 成为技术， 业务模范（标兵）
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->kualitas_kerja == 49)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;">
                                            <b> {{ $data->kualitas_kerja }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Kualitas kerja tidak stabil namun dapat bekerja sama dengan tim
                                            <br/>
                                            <span class="chinesse">
                                                工作质量不稳定但与团队合作能力强
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->kualitas_kerja == 28)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Kualitas kerja kurang bagus dan tidak dapat berkoordinasi dalam bekerja
                                            <br/>
                                            <span class="chinesse">
                                                工作质量不太好， 没有跟其他相关工作协调
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->kualitas_kerja == 33)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Kualitas kerja bagus serta tidak terdapat revisi atau pengulangan kerja
                                            <br/>
                                            <span class="chinesse">
                                                工作质量良好，没有返工现象
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->kualitas_kerja == 43.5)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Kualitas kerja kurang bagus terkadang terdapat revisi atau pengulangan kerja.
                                            <br/>
                                            <span class="chinesse">
                                                工作质量不太好， 有时有返工现象
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->kualitas_kerja == 37.5)
                                                    &#10004;
                                                @endif</center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5">
                                            <span class="first-line">
                                                3. Efisiensi Kerja
                                            </span><br/>
                                            <span class="hanging chinesse">
                                                工作效率
                                            </span>
                                        </td>
                                        <td>
                                            Tidak mampu dalam mencapai target kerja serta tidak memiliki rencana kerja
                                            <br/>
                                            <span class="chinesse">
                                                达不到工作要求也没有工作计划
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->efesiensi_kerja == 40)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;">
                                            <b>{{ $data->efesiensi_kerja }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mengerjakan pekerjaan sesuai terget kerja serta memiliki rencana kerja.
                                            <br/>
                                            <span class="chinesse">
                                                达到工作要求也有工作计划
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->efesiensi_kerja == 60)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Menyelesaikan pekerjaan sesuai target waktu serta target kerja terpenuhi
                                            <br/>
                                            <span class="chinesse">
                                                在规定时限内按照完成工作也工作有计划
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->efesiensi_kerja == 58)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ketepatan waktu penyelesaian pekerjaan kurang namun mampu mencapai
                                            target kerja
                                            <br/>
                                            <span class="chinesse">
                                                工作时效性稍逊， 能达到工作要求
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->efesiensi_kerja == 50)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Terkadang melakukan penundaan pekerjaan juga kurang dalam pencapaian
                                            target kerja
                                            <br/>
                                            <span class="chinesse">
                                                有时推迟工作工也达不到工作要求
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->efesiensi_kerja == 48)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5">
                                            <span class="first-line">
                                                4. Kesadaran Keselamatan Dalam Berkerja
                                            </span><br/>
                                            <span class="hanging chinesse">
                                                安全作业
                                            </span>
                                        </td>
                                        <td>
                                            Menggunakan APD saat bekerja juga mampu memberikan pemahaman kepada rekan
                                            kerja terkait pentingnya keselamatan kerja
                                            <br/>
                                            <span class="chinesse">
                                                有很强的安全意识, 进入工作现场穿戴齐全劳保用品, 能为班组提供安全意识
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->keselamatan_dalam_kerja == 35)
                                                    &#10004;
                                                @endif  </center>
                                        </td>
                                        <td rowspan="5" style="text-align: center; vertical-align: middle;">
                                            <b> {{ $data->keselamatan_dalam_kerja }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tidak mengabaikan keselamatan kerja dan selalu patuh terkait penggunaan
                                            APD lengkap saat bekerja
                                            <br/>
                                            <span class="chinesse">
                                                基本有安全意识每进入工作现场穿戴齐全劳保用品
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->keselamatan_dalam_kerja == 32.5)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tidak acuh pada keselematan kerja namun mengenakan APD pada saat bekerja
                                            <br/>
                                            <span class="chinesse">
                                                安全意识薄弱但进入工作现场穿戴劳保用品
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->keselamatan_dalam_kerja == 17)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Memiliki kesadaran akan keselamatan kerja dan mengetahui aturan dalam
                                            penggunaan APD lengkap saat bekerja
                                            <br/>
                                            <span class="chinesse">
                                                有较强安全意识， 进入工作现场穿戴齐全劳保用品
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->keselamatan_dalam_kerja == 20.5)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Memiliki kesadaran akan keselamatan kerja dan mengetahui aturan dalam
                                            penggunaan APD lengkap saat bekerja
                                            <br/>
                                            <span class="chinesse">
                                                有较强安全意识， 进入工作现场穿戴齐全劳保用品
                                            </span>
                                        </td>
                                        <td>
                                            <center>@if($data->keselamatan_dalam_kerja == 19)
                                                    &#10004;
                                                @endif </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <b>TOTAL PEROLEHAN EVALUASI PENCAPAIAN KINERJA (A)<br/>
                                                <span class="chinesse">绩效成就评价</span>
                                            </b>
                                        </td>
                                        <td>
                                            <center><b>{{ $data->total_nilai_kinerja }}</b></center>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end .table-responsive-->
                            <br>
                            <h5>II. EVALUASI PENCAPAIAN KERJA <br/>
                                <span class="hanging chinesse">工作成就评价</span></h5>
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <td style="text-align: center">
                                            BOBOT PENILAIAN (20%)<br/>
                                            <span class="chinesse">
                                                评估的百分比 (20%)
                                            </span>
                                        </td>
                                        <td style="text-align: center">
                                            KOLOM CENTANG<br/>
                                            <span class="chinesse">
                                                评分栏 (√)
                                            </span>
                                        </td>
                                        <td style="text-align: center">
                                            KOLOM NILAI (DIISI OLEH HRD)<br/>
                                            <span class="chinesse">
                                                成绩栏（人事审核人）
                                            </span>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <center>A+</center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->a_plus))&#10004;@endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->a_plus)){{ $data->a_plus }}@endif
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <center>A</center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->a))&#10004;@endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->a)){{ $data->a }}@endif
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <center>A-</center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->a_minus))&#10004;@endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->a_minus)){{ $data->a_minus }}@endif
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <center>B+</center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->b_plus))&#10004;@endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->b_plus)){{ $data->b_plus }}@endif
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <center>B</center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->b))&#10004;@endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->b)){{ $data->b }}@endif
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <center>B-</center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->b_minus))&#10004;@endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->b_minus)){{ $data->b_minus }}@endif
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <center>C+</center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->c_plus))&#10004;@endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->c_plus)) {{ $data->c_plus }}@endif
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <center>C</center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->c))&#10004;@endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->c)){{ $data->c }}@endif
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <center>C-</center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->c_minus))&#10004;@endif
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                @if(!empty($data->c_minus)) {{ $data->c_minus }}@endif
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                TOTAL PEROLEHAN EVALUASI PENCAPAIAN KERJA (B)<br/>
                                                <span class="chinesse">工作成果评价总获得</span>
                                            </b>
                                        </td>
                                        <td style=" text-align: center; vertical-align: middle;">
                                            <b>{{ $data->total_nilai_pencapaian }}</b>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            <!-- end row-->
        </div> <!-- container -->
    </div>
    <style>
        .chinesse {
            font-size: 13px;
            letter-spacing: 2px;
        }

        .indonesian {
            font-size: 12px;
        }

        .hanging {
            padding-left: 10px;
            display: inline-block;
        }

        .first-line {
            padding-left: 10px;
            display: inline-block;
            text-indent: -10px;
        }
    </style>
    @if(Auth::user()->level == "Administrator")
        @include('salary.form')
    @endif
@endsection

