@extends('layouts.app')
@section('content')
<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Pay Slip</a></li>
                            <li class="breadcrumb-item active">Kinerja Karyawan dan Pencapaian Kerja</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Kinerja Karyawan dan Pencapaian Kerja 员工绩效和工作成就</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                       <center><h5>HASIL KINERJA DAN PENCAPAIAN KERJA<br/><br/>员工绩效和工作成就</h5></center><br/>
                       <div class="table-responsive">
                           <table class="table table-bordered mb-0">
                               <tbody>
                               <tr>
                                   <td width="18%">
                                       NAMA <br/><span class="chinesse">员工姓名</span>
                                   </td>
                                   <td width="20%" style="text-align: left; vertical-align: middle">
                                       {{ $data->data_karyawan->nama }}
                                   </td>
                                   <td width="10%">
                                       PERUSAHAAN <br/><span class="chinesse">公司</span>
                                   </td>
                                   <td width="22%" style="text-align: left; vertical-align: middle">
                                       @if($data->data_karyawan->nm_perusahaan === "OSS")
                                           PT. Obsidian Stainless Stell (OSS)
                                       @elseif($data->data_karyawan->nm_perusahaan === "PMS")
                                           PT. Pelabuhan Muara Sampara (PMS)
                                       @endif
                                   </td>
                                   <td width="10%">
                                       POSISI <br/><span class="chinesse">岗位</span>
                                   </td>
                                   <td width="15%" style="text-align: left; vertical-align: middle">
                                       @if($datasupport != null)
                                           {{ $datasupport->posisi }}
                                       @endif
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       NIK <br/><span class="chinesse">工号</span>
                                   </td>
                                   <td style="text-align: left; vertical-align: middle">
                                       {{ $data->data_karyawan->nik }}
                                   </td>
                                   <td>
                                       DEPARTEMEN <br/><span class="chinesse">大部门</span>
                                   </td>
                                   <td style="text-align: left; vertical-align: middle">
                                       @if($datasupport != null)
                                           {{ $datasupport->departement }}
                                       @endif
                                   </td>
                                   <td>
                                       NPWP <br/><span class="chinesse">交税号</span>
                                   </td>
                                   <td style="text-align: left; vertical-align: middle">
                                       {{ $data->data_karyawan->npwp }}
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       TANGGAL MASUK KERJA<br/>
                                       <span class="chinesse">入职时间</span>
                                   </td>
                                   <td style="text-align: left; vertical-align: middle">
                                       {{ date('d M Y', strtotime($data->data_karyawan->tgl_join)) }}
                                   </td>
                                   <td>
                                       DIVISI <br/><span class="chinesse">部门</span>
                                   </td>
                                   <td style="text-align: left; vertical-align: middle">
                                       @if($datasupport != null)
                                           {{ $datasupport->divisi }}
                                       @endif
                                   </td>
                                   <td>
                                       &nbsp;
                                   </td>
                                   <td>
                                       &nbsp;
                                   </td>
                               </tr>
                               </tbody>
                           </table>
                    </div> <!-- end .table-responsive-->
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                            <h5> III. EVALUASI KETENAGAKERJAAN <span class="chinesse">就业评估</span>(C)</h5>
                                    <div class="table-responsive">
                                        <table class="table table-bordered mb-0">
                                            <thead>
                                                <tr>
                                                    <td rowspan="2" class="center_middle">
                                                        ASPEK EVALUASI<br/>
                                                        <span class="chinesse">评估项目</span>
                                                    </td>
                                                    <td rowspan="2" class="center_middle">
                                                        UNSUR EVALUASI<br/>
                                                        <span class="chinesse">评估要素</span>
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        NILAI PER UNSUR EVALUASI<br/>
                                                        <span class="chinesse">每个评估要素的价值</span>
                                                    </td>
                                                    <td rowspan="2" class="center_middle">
                                                        NILAI PER KATEGORI<br/>
                                                        <span class="chinesse">每个类别的价值</span>
                                                    </td>
                                                    <td rowspan="2" class="center_middle">
                                                        TOTAL NILAI<br/>
                                                        <span class="chinesse">总价值</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center_middle">
                                                        Jumlah Unsur Evaluasi (Hari)<br/>
                                                        <span class="chinesse">评价要素数 (天)</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        Poin Nilai<br/>
                                                        <span class="chinesse">价值点</span>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td rowspan="9" class="center_middle">
                                                        <b>Evaluasi Kehadiran</b><br/>
                                                        <span class="chinesse">考勤评估</span>

                                                    </td>
                                                    <td>
                                                        Total Kehadiran Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年总出勤率</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_kehadiran }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->poin_nilai_kehadiran }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->kategori_kehadiran }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_kehadiran }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Alfa Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度矿工合计</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_alfa }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->poin_nilai_alfa }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->kategori_alfa }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_alfa }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Sakit Dalam 1 Tahun<br/>
                                                        <span class="spacing">
                                                            一年度病假
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_sakit }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->poin_nilai_sakit }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->kategori_sakit }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_sakit }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Izin Paidleave Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度请假</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_paidleave }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->poin_nilai_paidleave }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->kategori_paidleave }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_paidleave }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Izin Unpaidleave Dalam 1 Tahun<br/>
                                                        <span class="spacing">
                                                            一年度事假
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_unpaidleave }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->poin_nilai_unpaidleave }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->kategori_unpaidleave }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_unpaidleave }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Keterlambatan Dalam 1 Tahun<br/>
                                                        <span class="spacing">
                                                            一年度迟到合计
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_keterlambatan }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->poin_nilai_keterlambatan }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->kategori_keterlambatan }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_keterlambatan }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Total Pulang Cepat Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度早退合计</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_pulang_cepat }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->poin_nilai_cepat }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->kategori_pulang_cepat }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_pulang_cepat }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Cuti Dalam 1 Tahun <br/>
                                                        <span class="spacing">
                                                            一年度年假合计
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_cuti }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->poin_nilai_cuti }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->kategori_cuti }}
                                                        @if($data->kategori_cuti === "SESUAI")
                                                            <br/>根据
                                                        @endif
                                                        @if($data->kategori_cuti === "TIDAK SESUAI")
                                                            <br/>它不符合
                                                        @endif
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_cuti }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total OFF Dalam 1 Tahun<br/>
                                                        <span class="spacing">
                                                            一年度休息合计
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_off }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->poin_nilai_off }}
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->kategori_off }}
                                                        @if($data->kategori_off === "SESUAI")
                                                            <br/>根据
                                                        @endif
                                                        @if($data->kategori_off === "TIDAK SESUAI")
                                                            <br/>它不符合
                                                        @endif
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_off }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td rowspan="6" class="center_middle">
                                                        <b>
                                                            Evaluasi Pelanggaran<br/>
                                                            <span class="spacing">
                                                                违规评估
                                                            </span>
                                                        </b>
                                                    </td>
                                                    <td>
                                                        Total SP 1 Dalam 1 Tahun<br/>
                                                        <span class="spacing">
                                                            一年度收一级警告次数
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_sp1 }}</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_sp1 }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total SP 2 Dalam 1 Tahun <br/>
                                                        <span class="spacing">
                                                            一年度收二级警告次数
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_sp2 }}</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_sp2 }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total SP 3 Dalam 1 Tahun <br/>
                                                        <span class="spacing">
                                                            一年度收三级警告次数
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_sp3 }}
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_sp3 }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Surat Pernyataan<br/>
                                                        <span class="spacing">
                                                            写保证书的次数
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_surat_pernyataan}}
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td class="center_middle">
                                                        {{$data->nilai_surat_pernyataan}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Denda<br/>
                                                        <span class="spacing">
                                                            考核和赔钱
                                                        </span>
                                                    </td>
                                                    <td class="center_middle">
                                                        {{ $data->jml_denda }}</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td class="center_middle">
                                                        {{ $data->nilai_denda }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Pelanggaran Lainnya<br/>
                                                        <span class="spacing">
                                                            违反其他公司规定
                                                        </span>
                                                    </td>
                                                    <td colspan="2" class="left_middle">
                                                        @if(!empty($data->pelanggaran_dikembalikan_kehrd))
                                                            - Pelanggaran Dikembalikan ke HRD<br/>
                                                            <span class="chinesse"> 违规行为返回给人力资源开发部</span>
                                                        @endif
                                                        @if(!empty($data->pelanggaran_dikembalikan_kehrd) &&
                                                        !empty($data->pelanggaran_tidak_memiliki_npwp))
                                                            <br/>
                                                        @endif
                                                        @if(!empty($data->pelanggaran_tidak_memiliki_npwp))
                                                            - Pelanggaran Tidak Memiliki NPWP<br/>
                                                            <span class="chinesse">&nbsp;&nbsp;违规行为没有税号</span>
                                                        @endif
                                                    </td>
                                                    <td></td>
                                                    <td class="center_middle">
                                                        {{ $data->pelanggaran_lainnya }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="left_middle">
                                                        <b>
                                                            TOTAL PEROLEHAN EVALUASI KETENAGAKERJAAN (C) <br/>
                                                            <span class="spacing">
                                                                就业评估的总收益
                                                            </span>
                                                        </b>
                                                    </td>
                                                    <td class="center_middle">
                                                        @if( $data->total_nilai < 0) 0
                                                        @else {{ $data->total_nilai }} @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <b>
                                            <i>
                                                Catatan :<br/>
                                                <span class="spacing">笔记</span>: <br/>
                                                A. Semua unsur evaluasi pelanggaran tidak memiliki nilai<br/>
                                                <span class="spacing">
                                                    &nbsp;&nbsp;&nbsp;违规评估的所有要素都没有价值
                                                </span><br/>
                                                B. Total perolehan evaluasi ketenagakerjaan (c) yang bernilai
                                                minus (-) akan bernilai 0 (nol) <br/>
                                                <span class="spacing">
                                                    &nbsp;&nbsp;&nbsp;就业评估的总收益 (c) 有负值 (-) 将价值 0 (零)
                                                </span>
                                            </i>
                                        </b>
                                        <br>
                                    </div> <!-- end .table-responsive-->

                        <br>

                            <h5>TABEL ACUAN<br/>
                                <span class="chinesse">参考表</span>
                            </h5>
                                    <div class="table-responsive">
                                        <table class="table table-bordered mb-0">
                                            <thead>
                                                <tr>

                                                    <td rowspan="2" class="center_middle">
                                                        UNSUR EVALUASI<br/>
                                                        <span class="spacing">评估要素</span>
                                                    </td>
                                                    <td colspan="4" class="center_middle">
                                                        POIN PER KATEGORI<br/>
                                                        <span class="spacing">每个类别的价值</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="center_middle">
                                                        Kurang (5)<br/>
                                                        <span class="spacing">差 (5)</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        Cukup (15)<br/>
                                                        <span class="spacing">充足 (15)</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        Baik (30)<br/>
                                                        <span class="spacing">良好 (30)</span>
                                                    </td>
                                                    <td align="center">
                                                        Sangat Baik (50)<br/>
                                                        <span class="spacing">优秀 (50)</span>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Total Kehadiran Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年总出勤率</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        1-259
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 260
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 304
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 335</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Alfa Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度矿工合计</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 6
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 5</td>
                                                    <td class="center_middle">
                                                        <= 2</td>
                                                    <td class="center_middle">
                                                        0
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Sakit Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度病假</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 10</td>
                                                    <td class="center_middle">
                                                        <= 9
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 5
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 3
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Izin Paidleave Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度请假</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 10
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 9
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 5
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 3
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                        Total Izin Unpaidleave Dalam 1 Tahun<br/>
                                                        <span class="center_middle">一年度事假</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 6</td>
                                                    <td class="center_middle">
                                                        <= 5
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 2
                                                    </td>
                                                    <td class="center_middle">
                                                        0
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Keterlambatan Dalam 1 Tahun<br/>
                                                        <span class="center_middle">一年度迟到合计</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 8
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 7
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 3
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 0
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Pulang Cepat Dalam 1 Tahun <br/>
                                                        <span class="spacing">一年度早退合计</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 8
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 7
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 3
                                                    </td>
                                                    <td class="center_middle">
                                                        <= 0
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total Cuti Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度年假合计</span>
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        Sesuai Dengan Hak Cuti (40)<br/>
                                                        <span class="spacing">不超过分配的年假 (40)</span>
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        Melebihi Hak Cuti (20)<br/>
                                                        <span class="spacing">超过分配的年假 (20)</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total OFF Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度休息合计</span>
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        Sesuai Dengan Hak OFF (40)<br/>
                                                        <span class="spacing">不超过分配的休息 (40)</span>
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        Melebihi Hak OFF (20)<br/>
                                                        <span class="spacing">超过分配的休息 (20)</span>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total SP 1 Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度收一级警告次数</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 2
                                                    </td>
                                                    <td class="center_middle">
                                                        1
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        0
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Total SP 2 Dalam 1 Tahun <br/>
                                                        <span class="spacing">一年度收二级警告次数</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 1
                                                    </td>
                                                    <td colspan="3" class="center_middle">
                                                        0
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        Total SP 3 Dalam 1 Tahun<br/>
                                                        <span class="spacing">一年度收三级警告次数</span>
                                                    </td>
                                                    <td class="center_middle">
                                                        >= 1
                                                    </td>
                                                    <td colspan="3" class="center_middle">
                                                        0
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Surat Pernyataan<br/>
                                                        <span class="spacing">写保证书的次数</span>
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        Ada<br/>
                                                        <span class="spacing">有</span>
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        Tidak Ada<br/>
                                                        <span class="spacing">没有</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Denda<br/>
                                                        <span class="spacing">考核和赔钱</span>
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        Ada<br/>
                                                        <span class="spacing">有</span>
                                                    </td>
                                                    <td colspan="2" class="center_middle">
                                                        Tidak Ada<br/>
                                                        <span class="spacing">没有</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Pelanggaran Lainnya<br/>
                                                        <span class="spacing">考核和赔钱</span>
                                                    </td>
                                                    <td colspan="4" class="left_middle">
                                                        - Pelanggaran Dikembalikan ke HRD<br/>
                                                        <span class="chinesse">&nbsp;&nbsp;违规行为返回给人力资源开发部</span><br/>
                                                        - Pelanggaran Tidak Memiliki NPWP<br/>
                                                        <span class="chinesse">&nbsp;&nbsp;违规行为没有税号</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> <!-- end .table-responsive-->
                            </div>
                        </div>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- container -->
</div>
<style>
    .chinesse {
        font-size: 13px;
        letter-spacing: 2px;
    }
    .spacing {
        letter-spacing: 2px;
    }
    .center_middle {
        text-align: center !important;
        vertical-align: middle !important;
    }
    .left_middle {
        text-align: left !important;
        vertical-align: middle !important;
    }
</style>
@if(Auth::user()->level == "Administrator")
@include('salary.form')
@endif
@endsection

