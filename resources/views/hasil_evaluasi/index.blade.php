@extends('layouts.app')
@section('content')
<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Pay Slip</a></li>
                            <li class="breadcrumb-item active">Hasil Evaluasi</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Hasil Evaluasi</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <table id="state-saving-datatable" class="table dt-responsive nowrap w-100">
                            <thead>
                                <tr>
                                    <th style="text-align: left; vertical-align: middle;">
                                        NIK<br/><span class="chinesse">工号</span>
                                    </th>
                                    <th style="text-align: left; vertical-align: middle;">
                                        Pencapaian Kinerja (A)<br/>
                                        <span class="chinesse">绩效成就评价 (A)</span>
                                    </th>
                                    <th style="text-align: left; vertical-align: middle;">
                                        Pencapaian Kerja (B)<br/>
                                        <span class="chinesse">工作成果评价总获得 (B)</span>
                                    </th>
                                    <th style="text-align: left; vertical-align: middle;">
                                        Patuh Ketenagakerjaan (C)<br/>
                                        <span class="chinesse">就业评估 (C)</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $d)
                                <tr>
                                    <td>
                                        <a href="{{ route('hasil-evaluasi.show', $d->id) }}" target="_blank">
                                            {{ $d->data_karyawan->nik }}
                                        </a>
                                    </td>
                                    <td>
                                        {{ $d->total_nilai_kinerja }}
                                    </td>
                                    <td>
                                        {{ $d->total_nilai_pencapaian }}
                                    </td>
                                    @foreach ($d->data_karyawan->evaluasi_ketenagakerjaan as $evaluasi)
                                    <td>
                                        @if($evaluasi->nilai_terkonversi > 0)
                                            {{ number_format(round($evaluasi->nilai_terkonversi,3),3) }}
                                        @else
                                            {{$evaluasi->nilai_terkonversi}}
                                        @endif
                                    </td>
                                    @endforeach
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- container -->
</div>
<style>
    .chinesse {
        font-size: 13px;
        letter-spacing: 2px;
    }
</style>
@if(Auth::user()->level == "Administrator")
@include('salary.form')
@endif
@endsection

