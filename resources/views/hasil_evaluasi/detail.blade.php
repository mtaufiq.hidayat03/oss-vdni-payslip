@extends('layouts.app')
@section('content')
<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Pay Slip</a></li>
                            <li class="breadcrumb-item active">Hasil Evaluasi</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Hasil Evaluasi <span style="letter-spacing: 2px">评价结果</span></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                       <center><h5>HASIL EVALUASI<br/><span class="chinesse">评价结果</span></h5></center>
                       <div class="table-responsive">
                        <table class="table table-bordered mb-0">
                            <tbody>
                                <tr>
                                    <td width="18%">
                                        NAMA <br/><span class="chinesse">员工姓名</span>
                                    </td>
                                    <td width="20%" style="text-align: left; vertical-align: middle">
                                        {{ $data->data_karyawan->nama }}
                                    </td>
                                    <td width="10%">
                                        PERUSAHAAN <br/><span class="chinesse">公司</span>
                                    </td>
                                    <td width="22%" style="text-align: left; vertical-align: middle">
                                        @if($data->data_karyawan->nm_perusahaan === "OSS")
                                            PT. Obsidian Stainless Stell (OSS)
                                        @elseif($data->data_karyawan->nm_perusahaan === "PMS")
                                            PT. Pelabuhan Muara Sampara (PMS)
                                        @endif
                                    </td>
                                    <td width="10%">
                                        POSISI <br/><span class="chinesse">岗位</span>
                                    </td>
                                    <td width="15%" style="text-align: left; vertical-align: middle">
                                        @if($datasupport != null)
                                            {{ $datasupport->posisi }}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        NIK <br/><span class="chinesse">工号</span>
                                    </td>
                                    <td style="text-align: left; vertical-align: middle">
                                        {{ $data->data_karyawan->nik }}
                                    </td>
                                    <td>
                                        DEPARTEMEN <br/><span class="chinesse">大部门</span>
                                    </td>
                                    <td style="text-align: left; vertical-align: middle">
                                        @if($datasupport != null)
                                            {{ $datasupport->departement }}
                                        @endif
                                    </td>
                                    <td>
                                        NPWP <br/><span class="chinesse">交税号</span>
                                    </td>
                                    <td style="text-align: left; vertical-align: middle">
                                        {{ $data->data_karyawan->npwp }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        TANGGAL MASUK KERJA<br/>
                                        <span class="chinesse">入职时间</span>
                                    </td>
                                    <td style="text-align: left; vertical-align: middle">
                                        {{ date('d M Y', strtotime($data->data_karyawan->tgl_join)) }}
                                    </td>
                                    <td>
                                        DIVISI <br/><span class="chinesse">部门</span>
                                    </td>
                                    <td style="text-align: left; vertical-align: middle">
                                        @if($datasupport != null)
                                            {{ $datasupport->divisi }}
                                        @endif
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> <!-- end .table-responsive-->
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5> HASIL EVALUASI<br/><span class="chinesse">评价结果</span></h5>
                        <div class="table-responsive">
                            <table class="table table-bordered mb-0">
                               <thead>
                                   <tr>
                                       <td class="center_middle">
                                           NO<br/>
                                           <span class="chinesse">数</span>
                                       </td>
                                       <td>
                                           POIN PENILAIAN<br/>
                                           <span class="chinesse">得分点</span>
                                       </td>
                                       <td class="center_middle">
                                           TOTAL NILAI DIPEROLEH<br/>
                                           <span class="chinesse">获得的总价值</span>
                                       </td>
                                       <td class="center_middle">
                                           PRESENTASI KATEGORI PENILAIAN<br/>
                                           <span class="chinesse">评估类别的介绍</span>
                                       </td>
                                       <td class="center_middle">
                                           HASIL NILAI TERKONVERSI<br/>
                                           <span class="chinesse">转换后的价值结果</span>
                                       </td>
                                   </tr>
                               </thead>
                               <tbody>
                                   <tr>
                                       <td class="center_middle">1</td>
                                       <td>
                                           EVALUASI PENCAPAIAN KINERJA (A)<br/>
                                           <span class="chinesse">绩效成就评价 (A)</span>
                                       </td>
                                       <td class="center_middle">
                                           {{ $data->total_nilai_kinerja  }}
                                       </td>
                                       <td class="center_middle">
                                           45%
                                       </td>
                                       <td class="center_middle">
                                           {{ number_format(round(
                                                $data->total_nilai_kinerja / 413.5 * 910.35, 3) ,3 ,',' ,'.')
                                           }}
                                       </td>
                                   </tr>
                                   <tr>
                                        <td class="center_middle">2</td>
                                        <td>
                                            EVALUASI PENCAPAIAN KERJA (B)<br/>
                                            <span class="chinesse">
                                                工作成果评价总获得 (B)
                                            </span>
                                        </td>
                                        <td class="center_middle">
                                            {{ $data->total_nilai_pencapaian }}
                                        </td>
                                        <td class="center_middle">
                                            20%
                                        </td>
                                        <td class="center_middle">
                                            {{ number_format(round(
                                                $data->total_nilai_pencapaian / 20 * 404.60, 3), 3)
                                            }}
                                        </td>
                                   </tr>
                                   @foreach ($data->data_karyawan->evaluasi_ketenagakerjaan as $evaluasi)
                                   <?php
                                     if($evaluasi->total_nilai < 0) {
                                         $evaluasi_tot = 0;
                                     } else {
                                         $evaluasi_tot = $evaluasi->total_nilai;
                                     }
                                   ?>
                                   <tr>
                                        <td class="center_middle">3</td>
                                        <td>
                                            EVALUASI PATUH KETENAGAKERJAAN(C)<br/>
                                            <span class="chinesse">
                                                雇佣合规评估 (C)
                                            </span>
                                        </td>
                                        <td class="center_middle">
                                            {{ $evaluasi_tot }}
                                        </td>
                                        <td class="center_middle">
                                            35%
                                        </td>
                                        <td class="center_middle">
                                            {{ number_format(round($evaluasi_tot / 950 * 708.05, 3), 3) }}
                                        </td>
                                  </tr>
                                  <tr>
                                      <td colspan="4" class="left_middle">
                                          <b>TOTAL NILAI KESELURUHAN</b><br/>
                                          <span class="chinesse">总价值</span>
                                      </td>
                                      <td class="center_middle">
                                          <b>
                                              {{ number_format(round(($data->total_nilai_kinerja / 413.5 * 910.35)
                                                + ($data->total_nilai_pencapaian / 20 * 404.60) +
                                                ($evaluasi_tot / 950 * 708.05), 3), 3)  }}
                                          </b>
                                      </td>
                                  </tr>
                                  @endforeach
                               </tbody>
                            </table>
                        </div> <!-- end .table-responsive-->
                        <br>
                        @if ($data->data_karyawan->nm_perusahaan  === "OSS")
                            <h5>
                                HASIL AKHIR PENILAIAN<br/>
                                <span class="chinesse">最终评估结果</span>
                            </h5>
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <td class="left_middle">
                                            NILAI AKHIR KONVERSI<br/>
                                            <span class="chinesse">转换最终价值</span>
                                        </td>
                                        <td class="left_middle">
                                            KATEGORI<br/>
                                            <span class="chinesse">类别</span>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                            <?php
                                            $hasil =
                                                ($data->total_nilai_kinerja / 413.5 * 910.35) +
                                                ($data->total_nilai_pencapaian / 20 * 404.60) +
                                                ($evaluasi_tot / 950 * 708.05);
                                            $convert = number_format(round($hasil,3), 3);
                                            ?>
                                        <td width="50%" class="left_middle">
                                            <b><?php echo $convert;?></b>
                                        </td>
                                        <td width="50%" class="left_middle">
                                            <b>
                                                @if($hasil >= 1901.73 && $hasil<= 2023.0)
                                                    SANGAT BAIK<br/>
                                                    <span class="chinesse">优秀</span>
                                                @elseif ($hasil >= 1719.53 && $hasil<= 1901.72)
                                                    BAIK<br/>
                                                    <span class="chinesse">良好</span>
                                                @elseif ($hasil >= 1213.83 && $hasil <= 1719.52)
                                                    CUKUP<br/>
                                                    <span class="chinesse">充足</span>
                                                @elseif ($hasil >= 0 && $hasil <= 1213.82)
                                                    KURANG<br/>
                                                    <span class="chinesse">差</span>
                                                @endif
                                            </b>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end .table-responsive-->
                            <br>
                            <h5>
                                RANGE KATEGORI PENILAIAN<br/>
                                <span class="chinesse">评级类别范围</span>
                            </h5>
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <td>
                                            RANGE NILAI PENILAIAN<br/>
                                            <span class="chinesse">评级类别范围</span>
                                        </td>
                                        <td>
                                            KATEGORI<br/>
                                            <span class="chinesse">类别</span>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1901.73 - 2023.0</td>
                                        <td>
                                            SANGAT BAIK<br/>
                                            <span class="chinesse">优秀</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1719.53 - 1901.72</td>
                                        <td>
                                            BAIK<br/>
                                            <span class="chinesse">良好</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1213.83 - 1719.52</td>
                                        <td>
                                            CUKUP<br/>
                                            <span class="chinesse">充足</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>0 - 1213.82</td>
                                        <td>
                                            KURANG<br/>
                                            <span class="chinesse">差</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end .table-responsive-->
                        @elseif($data->data_karyawan->nm_perusahaan  === "PMS")
                            <h5>
                                HASIL AKHIR PENILAIAN<br/>
                                <span class="chinesse">最终评估结果</span>
                            </h5>
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <td class="left_middle">
                                            NILAI AKHIR KONVERSI<br/>
                                            <span class="chinesse">转换最终价值</span>
                                        </td>
                                        <td class="left_middle">
                                            KATEGORI<br/>
                                            <span class="chinesse">类别</span>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                            <?php
                                            $hasil =
                                                ($data->total_nilai_kinerja / 413.5 * 910.35) +
                                                ($data->total_nilai_pencapaian / 20 * 404.60) +
                                                ($evaluasi_tot / 950 * 708.05);
                                            $convert = number_format(round($hasil,3), 3);
                                            ?>
                                        <td width="50%" class="left_middle">
                                            <b><?php echo $convert;?></b>
                                        </td>
                                        <td width="50%" class="left_middle">
                                            <b>
                                                @if($hasil >= 1901.73 && $hasil<= 2023.0)
                                                    SANGAT BAIK<br/>
                                                    <span class="chinesse">优秀</span>
                                                @elseif ($hasil >= 1679.63 && $hasil<= 1901.72)
                                                    BAIK<br/>
                                                    <span class="chinesse">良好</span>
                                                @elseif ($hasil >= 1213.83 && $hasil <= 1679.62)
                                                    CUKUP<br/>
                                                    <span class="chinesse">充足</span>
                                                @elseif ($hasil >= 0 && $hasil <= 1213.82)
                                                    KURANG<br/>
                                                    <span class="chinesse">差</span>
                                                @endif
                                            </b>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end .table-responsive-->
                            <br>
                            <h5>
                                RANGE KATEGORI PENILAIAN<br/>
                                <span class="chinesse">评级类别范围</span>
                            </h5>
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <td>
                                            RANGE NILAI PENILAIAN<br/>
                                            <span class="chinesse">评级类别范围</span>
                                        </td>
                                        <td>
                                            KATEGORI<br/>
                                            <span class="chinesse">类别</span>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1901.73 - 2023.0</td>
                                        <td>
                                            SANGAT BAIK<br/>
                                            <span class="chinesse">优秀</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1679.63 - 1901.72</td>
                                        <td>
                                            BAIK<br/>
                                            <span class="chinesse">良好</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1213.83 - 1679.62</td>
                                        <td>
                                            CUKUP<br/>
                                            <span class="chinesse">充足</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>0 - 1213.82</td>
                                        <td>
                                            KURANG<br/>
                                            <span class="chinesse">差</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end .table-responsive-->
                        @endif
                        <br>
                          <h5>
                              TABEL ACUAN<br/>
                              <span class="chinesse">参考表</span>
                          </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered mb-0">
                               <thead>
                                   <tr>
                                       <td>
                                           NO<br/>
                                           <span class="chinesse">数</span>
                                       </td>
                                       <td>
                                           POIN PENILAIAN<br/>
                                           <span class="chinesse">得分点</span>
                                       </td>
                                       <td>
                                           NILAI MAKSIMAL<br/>
                                           <span class="chinesse">极点</span>
                                       </td>
                                       <td>
                                           NILAI KONVERSI<br/>
                                           <span class="chinesse">转化价值</span>
                                       </td>
                                   </tr>
                               </thead>
                               <tbody>
                                   <tr>
                                       <td>1</td>
                                       <td>
                                           EVALUASI PENCAPAIAN KINERJA(A)<br/>
                                           <span class="chinesse">绩效成就评价 (A)</span>
                                       </td>
                                       <td>
                                           413.5
                                       </td>
                                       <td>
                                           910.35
                                       </td>
                                   </tr>
                                    <tr>
                                       <td>2</td>
                                       <td>
                                           EVALUASI PENCAPAIAN KERJA (B)<br/>
                                           <span class="chinesse">工作成果评价总获得 (B)</span>
                                       </td>
                                       <td>
                                           20
                                       </td>
                                       <td>
                                           404.60
                                       </td>
                                   </tr>
                                    <tr>
                                       <td>3</td>
                                       <td>
                                           EVALUASI PATUH KETENAGAKERJAAN (C)<br/>
                                           <span class="chinesse">就业评估 (C)</span>
                                       </td>
                                       <td>
                                           950
                                       </td>
                                       <td>
                                           708.50
                                       </td>
                                   </tr>
                               </tbody>
                            </table>
                        </div> <!-- end .table-responsive-->
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->

            <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5>
                            TATA CARA PENILAIAN HASIL AKHIR<br/>
                            <span class="chinesse">最终结果评估程序</span>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered mb-0">
                               <thead>
                                   <tr>
                                       <td width="3%" class="center_middle">
                                           NO<br/>
                                           <span class="chinesse">数</span>
                                       </td>
                                       <td width="27%" class="center_middle">
                                           POIN PENILAIAN<br/>
                                           <span class="chinesse">得分点</span>
                                       </td>
                                       <td width="10%" class="center_middle">
                                           TOTAL NILAI DIPEROLEH<br/>
                                           <span class="chinesse">赚取的总价值</span>
                                       </td>
                                       <td width="5%" class="center_middle">
                                           PRESENTASE KATEGORI PENILAIAN<br/>
                                           <span class="chinesse">评估类别的介绍</span>
                                       </td>
                                       <td width="35%" class="center_middle">
                                           CARA PERHITUNGAN<br/>
                                           <span class="chinesse">计算方法</span>
                                       </td>
                                       <td width="15%" class="center_middle">
                                           HASIL NILAI TERKONVERSI<br/>
                                           <span class="chinesse">转换后的价值结果</span>
                                       </td>
                                   </tr>
                               </thead>
                               <tbody>
                                   <tr>
                                       <td>1</td>
                                       <td>
                                           EVALUASI PENCAPAIAN KINERJA(A)<br/>
                                           <span class="chinesse">绩效成就评价 (A)</span>
                                       </td>
                                       <td class="center_up">350</td>
                                       <td class="center_up">45%</td>
                                       <td>
                                           {Total Nilai Diperoleh(A) : Nilai Maksimal(A)} x Nilai Konversi(A) = NILAI (A)<br/>
                                           <span class="chinesse">
                                               {获得的总价值(A) : 最大值(A)} x 兑换率(A) = 分数 (A)
                                           </span>
                                       </td>
                                       <td>NILAI (A)<br/><span class="chinesse">分数 (A)</span></td>
                                   </tr>
                                    <tr>
                                       <td>2</td>
                                       <td>
                                           EVALUASI PENCAPAIAN KERJA (B)<br/>
                                           <span class="chinesse">工作成果评价总获得 (B)</span>
                                       </td>
                                       <td class="center_up">20</td>
                                       <td class="center_up">20%</td>
                                       <td>
                                           {Total Nilai Diperoleh (B) : Nilai Maksimal (B)} x Nilai Konversi (B) = NILAI (B)<br/>
                                           <span class="chinesse">
                                               {获得的总价值(B) : 最大值(B)} x 兑换率(B) = 分数 (B)
                                           </span>
                                       </td>
                                       <td>NILAI (B)<br/><span class="chinesse">分数 (B)</span></td>
                                   </tr>
                                    <tr>
                                       <td>3</td>
                                       <td>
                                           EVALUASI PATUH KETENAGAKERJAAN (C)<br/>
                                           <span class="chinesse">就业评估 (C)</span>
                                       </td>
                                       <td class="center_up">730</td>
                                       <td class="center_up">35%</td>
                                       <td>
                                           {Total Nilai Diperoleh (C) : Nilai Maksimal (C)} x Nilai Konversi (C) = NILAI (C)<br/>
                                           <span class="chinesse">
                                               {获得的总价值(C) : 最大值(C)} x 兑换率(C) = 分数 (C)
                                           </span>
                                       </td>
                                       <td>NILAI (C)<br/><span class="chinesse">分数 (C)</span></td>
                                   </tr>
                                    <tr>
                                       <td colspan="4">
                                           TOTAL NILAI KESELURUHAN <br/>
                                           <span class="chinesse">总价值</span>
                                       </td>
                                       <td>NILAI ( A + B + C ) = TOTAL NILAI KESELURUHAN<br/>
                                           <span class="chinesse">
                                               分数 ( A + B + C )  = 总价值
                                           </span>
                                       </td>
                                       <td>HASIL AKHIR PENILAIAN<br/>
                                           <span class="chinesse">最终评估结果</span>
                                       </td>
                                   </tr>

                               </tbody>
                            </table>
                        </div> <!-- end .table-responsive-->
                        <br>
                        <h5>
                            CONTOH CARA PENILAIAN HASIL AKHIR<br/>
                            <span class="chinesse">最终结果评估方法示例</span>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered mb-0">
                                <thead>
                                <tr>
                                    <td width="3%" class="center_middle">
                                        NO<br/>
                                        <span class="chinesse">数</span>
                                    </td>
                                    <td width="25%" class="center_middle">
                                        POIN PENILAIAN<br/>
                                        <span class="chinesse">得分点</span>
                                    </td>
                                    <td width="12%" class="center_middle">
                                        TOTAL NILAI DIPEROLEH<br/>
                                        <span class="chinesse">赚取的总价值</span>
                                    </td>
                                    <td width="12%" class="center_middle">
                                        PRESENTASE KATEGORI PENILAIAN<br/>
                                        <span class="chinesse">评估类别的介绍</span>
                                    </td>
                                    <td width="25%" class="center_middle">
                                        CARA PERHITUNGAN<br/>
                                        <span class="chinesse">计算方法</span>
                                    </td>
                                    <td width="10%" class="center_middle">
                                        HASIL NILAI TERKONVERSI<br/>
                                        <span class="chinesse">转换后的价值结果</span>
                                    </td>
                                </tr>
                                </thead>
                               <tbody>
                                   <tr>
                                       <td>1</td>
                                       <td>EVALUASI PENCAPAIAN KINERJA(A)</td>
                                       <td class="center_middle">350</td>
                                       <td class="center_middle">45%</td>
                                       <td>( 350 : 413.5 ) X 910.35 = 770.550</td>
                                       <td class="center_middle">770.550</td>
                                   </tr>
                                    <tr>
                                       <td>2</td>
                                       <td>EVALUASI PENCAPAIAN KERJA (B)</td>
                                       <td class="center_middle">20</td>
                                       <td class="center_middle">20%</td>
                                       <td>( 20 : 20 ) X 404.60 = 404.60</td>
                                       <td class="center_middle">
                                           404.60
                                       </td>
                                   </tr>
                                    <tr>
                                       <td>3</td>
                                       <td>EVALUASI PATUH KETENAGAKERJAAN (C)</td>
                                       <td class="center_middle">730</td>
                                       <td class="center_middle">35%</td>
                                       <td>(730 : 950) X 708.50 = 544.426</td>
                                       <td class="center_middle">
                                           544.426
                                       </td>
                                   </tr>
                                    <tr>
                                       <td colspan="4" class="left_middle">
                                           TOTAL NILAI KESELURUHAN<br/>
                                           <span class="chinesse">总价值</span>
                                       </td>
                                       <td class="left_middle">
                                           770.550 + 404.60 + 544.426 = 1,719.576
                                       </td>
                                       <td class="center_middle">
                                           1,719.576
                                       </td>
                                   </tr>
                               </tbody>
                            </table>
                        <br>
                        </div> <!-- end .table-responsive-->

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- container -->
</div>
<style>
    .chinesse {
        font-size: 13px;
        letter-spacing: 2px;
    }
    .center_middle {
        text-align: center !important;
        vertical-align: middle !important;
    }
    .left_middle {
        text-align: left !important;
        vertical-align: middle !important;
    }
    .center_up {
        text-align: center !important;
        vertical-align: top !important;
    }
</style>
@if(Auth::user()->level == "Administrator")
@include('salary.form')
@endif
@endsection

