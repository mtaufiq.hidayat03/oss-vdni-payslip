<?php

namespace App\Imports;

use App\Models\DataKaryawan;
use App\Models\KomponenGaji;
use App\Models\FailUploadKomponen;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;
use Carbon;


class HapusKaryawan implements ToModel, WithHeadingRow, SkipsOnError, withValidation, SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;

    private $niks;
    private $row = 2;

    public function __construct()
    {
        $this->niks = DataKaryawan::select('id','nik','no_ktp')->get();
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $karyawan = $this->niks->where('nik', $row['nik_karyawan'])->where('no_ktp', $row['nomor_ktp'])->first();
        if($karyawan === null) {
            FailUploadKomponen::create([
                'baris' => $this->row,
                'nik' => $row['nik_karyawan'],
                'no_ktp' => $row['nomor_ktp'],
            ]);
        } else {
            $karyawan->delete();
       }
    }
    public function rules(): array
    {
        return [
            '*.nik_karyawan' => ['required'],
            '*.nomor_ktp' => ['required'],
        ];
    }
}
