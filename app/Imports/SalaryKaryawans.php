<?php

namespace App\Imports;
use App\Models\DataKaryawan;
use App\Models\NewKomponen;
use App\Models\FailUploadKomponen;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;
Use Carbon;

class SalaryKaryawans implements ToModel, WithHeadingRow, SkipsOnError, withValidation, SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;

    private $niks;
    private $row = 2;

    public function __construct()
    {
        $this->niks = DataKaryawan::select('id','nik','no_ktp')->get();
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $karyawan = $this->niks->where('nik', $row['nik'])->where('no_ktp', $row['no_ktp'])->first();
        if($karyawan === null) {
            FailUploadKomponen::create([
                'baris' => $this->row,
                'nik' => $row['nik'],
                'no_ktp' => $row['no_ktp'],
            ]);
        } else {
        return new NewKomponen([
            'data_karyawan_id' => $karyawan->id ?? null,
            'departement' => $row['departement'],
            'divisi' => $row['divisi'],
            'posisi' => $row['posisi'],
            'status_salary' => $row['status_salary'],
            'durasi_sp' => Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['durasi_sp'])),
            'jml_hari_kerja' => $row['jml_hari_kerja'],
            'jml_hour_machine' => $row['jml_hour_machine'],
            'gaji_pokok' => $row['gapok'],
            'tunj_um' => $row['tunj_um'],
            'tunj_masa_kerja' => $row['tunj_masa_kerja'],
            'tunj_koefisien' => $row['tunj_koefisien'],
            'insentif_tidak_tetap' => $row['insentif_tdk_tetap'],
            'tunj_lapangan' => $row['tunj_lapangan'],
            'insentif' => $row['insentif'],
            'rapel' => $row['rapel'],
            'overtime' => $row['overtime'],
            'hour_machine' => $row['hour_machine'],
            'bonus' => $row['tunj_managerial'],
            'thr' => $row['thr'],
            'bpjs_tk_jht' => $row['bpjs_tk_jht'],
            'bpjs_tk_jp' => $row['bpjs_tk_jp'],
            'bpjs_kes' => $row['bpjs_kes'],
            'deduction' => $row['deduction'],
            'tot_diterima' => $row['tot_diterima'],
            'bank_name' => $row['bank_name'],
            'bank_number' => $row['bank_number'],
            'periode' => $row['periode']
        ]);
      }
      $this->row++;
    }

    public function rules(): array
    {
        return [
            '*.periode' => ['required'],
        ];
    }
}
