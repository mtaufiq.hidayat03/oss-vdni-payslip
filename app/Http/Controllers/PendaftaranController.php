<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\DataKaryawan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Yajra\DataTables\Datatables;
use App\Mail\RegisterEmail;
use Illuminate\Support\Facades\Mail;
use Auth;
use Alert;

class PendaftaranController extends Controller
{
    public function pendaftaran(Request $request)
    {
        $cek_data = DataKaryawan::where('nik',$request['nik'])
                    ->where('no_ktp', $request['no_ktp'])
                    ->where('tgl_lahir', $request['tgl_lahir'])
                    ->first();
        if($cek_data != null)
        {
            if($request['password'] == $request['confirm_password'])
            {
                $email = $request['email'];
                $password = Hash::make($request['password']);
                $status = "Tidak Aktif";
                $token = Str::random(60);
                $level = "Pengguna";
                $karyawan_id = $cek_data->id;

                $cek_user = User::where('email', $email)
                            ->orWhere('data_karyawan_id', $karyawan_id)
                            ->first();
                $cek_token = User::where('token', $token)->first();
                if($cek_token === null) {
                    if($cek_user === null)
                    {
                        User::create([
                            'name' => $cek_data->nama,
                            'email' => $email,
                            'password' => $password,
                            'level' => $level,
                            'status' => $status,
                            'data_karyawan_id' => $karyawan_id,
                            'token' => $token
                        ]);

                    $param = [
                        'nama' => $cek_data->nama,
                        'token' => $token
                    ];

                    //Mail::to($email)->send(new RegisterEmail($param));
                    Alert::success('Sukses', 'Pendaftaran Berhasil Silahkan Login Menggunakan Email dan Password Anda. Terimakasih');
                    return redirect()->route('login');
                } else {
                    Alert::error('Gagal', 'Maaf Alamat Email Atau User Anda Sudah Terdaftar!!');
                    return redirect()->route('register');
                }
            } else {
                Alert::error('Gagal', 'Maaf Terjadi kesalahan di sistem kami!!');
                return redirect()->route('register');
                }
            } else {
                Alert::error('Gagal', 'Maaf Password dan Konfirmasi Password Harus Sama!!!');
                return redirect()->route('register');
            }
        } else {
            Alert::error('Gagal', 'Maaf Data Anda Tidak Terdaftar Disistem Kami!!!');
            return redirect()->route('register');
        }
    }

    public function verifikasi($id)
    {
        $cek = User::where('token', $id)->first();
        if($cek === null)
        {
           return view('email.failed');
        } else if($cek->level == "Pengguna")
        {
            $cek->status = "Aktif";
            $cek->token = "";
            $cek->update();
            return view('email.verifikasi');
        } else
        {
           return view('email.failed');
        }
    }

}
