<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewKomponen extends Model
{
    protected $fillable = [
        'data_karyawan_id','departement','divisi','posisi','status_salary','durasi_sp','jml_hari_kerja','jml_hour_machine','gaji_pokok','tunj_um',
        'tunj_masa_kerja','tunj_koefisien','insentif_tidak_tetap','tunj_lapangan','insentif','rapel','overtime','hour_machine',
        'bonus','thr','bpjs_tk_jht','bpjs_tk_jp','bpjs_kes','deduction','tot_diterima','bank_name',
        'bank_number','periode'
    ];
    use HasFactory;


    public function data_karyawan() {
        return $this->belongsTo(DataKaryawan::class);
    }

    use HasFactory;
}
