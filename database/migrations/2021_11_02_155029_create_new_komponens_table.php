<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewKomponensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_komponens', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('data_karyawan_id');
            $table->foreign('data_karyawan_id')->references('id')->on('data_karyawans')->onDelete('cascade');
            $table->string('departement')->nullable();
            $table->string('divisi')->nullable();
            $table->string('posisi')->nullable();
            $table->string('status_salary')->nullable();
            $table->date('durasi_sp')->nullable();
            $table->string('jml_hari_kerja')->nullable();
            $table->string('jml_hour_machine')->nullable();
            $table->string('gaji_pokok')->nullable();
            $table->string('tunj_um')->nullable();
            $table->string('tunj_masa_kerja')->nullable();
            $table->string('tunj_koefisien')->nullable();
            $table->string('insentif_tidak_tetap')->nullable();
            $table->string('tunj_lapangan')->nullable();
            $table->string('insentif')->nullable();
            $table->string('rapel')->nullable();
            $table->string('overtime')->nullable();
            $table->string('hour_machine')->nullable();
            $table->string('bonus')->nullable();
            $table->string('thr')->nullable();
            $table->string('bpjs_tk_jht')->nullable();
            $table->string('bpjs_tk_jp')->nullable();
            $table->string('bpjs_kes')->nullable();
            $table->string('deduction')->nullable();
            $table->string('tot_diterima')->nullable();
            $table->string('bank_number')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('periode')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_komponens');
    }
}
